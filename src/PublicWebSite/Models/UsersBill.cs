﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace PublicWebSite.Models
{
    public class UsersBill:MainBaseEntity
    {
        public UsersBill()
        {
            this.InsertDate = DateTime.Now;
            Random rndRandom=new Random();
            this.SaleOrderId = rndRandom.Next(300, 65535);
        }

        public string Title { get; set; }
        public long SaleOrderId { get; set; }

        public double Sum { get; set; }
        public DateTime InsertDate { get; set; }
   
        public int? UserInfo_Id { get; set; }

    }
}