﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace PublicWebSite.Models
{
    public class ContentPage:MainBaseEntity
    {
        [Display(Name = "PageTitle", ResourceType = typeof(Resources.Common))]
        [Required(ErrorMessageResourceName = "TitleRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        public string Title { get; set; }
        [Display(Name = "PageBody", ResourceType = typeof(Resources.Common))]
        [AllowHtml]
        public string Body { get; set; }
        public string KeyWords { get; set; }
        public string PageName { get; set; }

    }
}