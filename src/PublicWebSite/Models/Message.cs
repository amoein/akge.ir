﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PublicWebSite.Models
{
    public class Message:BaseEntity
    {
        public Message()
        {
            this.IsViewed = false;
            user_id = 0;
        }
         [AllowHtml]
         [Display(Name = "MessageBody", ResourceType = typeof(Resources.Common))]
        public string Body { get; set; }

        [Required(ErrorMessageResourceName = "TitleRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        [Display(Name = "MessageTitle", ResourceType = typeof(Resources.Common))]
        public string Title { get; set; }
        public bool IsViewed { get; set; }
        public DateTime? ViewDate { get; set; }
        public int user_id{ get; set; }
        public virtual ICollection<UserInfo> UserInfos { get; set; }
    }
}