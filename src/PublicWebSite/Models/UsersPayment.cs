﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace PublicWebSite.Models
{
    public class UsersPayment:MainBaseEntity
    {

        public UsersPayment()
        {
            Random rndRandom = new Random();
            this.OrderId = rndRandom.Next(300, 65535);
        }
        public long OrderId { get; set; }
        public int UserInfo_Id { get; set; }
        public double Amount { get; set; }
        public string PayedDate { get; set; }
        public string rfid { get; set; }
        public int? ResponseCode { get; set; }

    }
}