﻿
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PublicWebSite.Models
{
    public class News: BaseEntity
    {
        [Display(Name = "NewsTitle", ResourceType = typeof(Resources.Common))]
        public string Title { get; set; }
        [Display(Name = "NewsBody", ResourceType = typeof(Resources.Common))]
        [AllowHtml]
        public string Body { get; set; }
        public string KeyWords { get; set; }

    }
}