﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PublicWebSite.Models
{
    public class BaseEntity:MainBaseEntity
    {
        public BaseEntity()
        {
            this.InsertDate = DateTime.Now;
            this.IsActive = true;
            this.ActiveDeActiveDate = DateTime.Now;
        }
        [Display(Name = "InsertDate", ResourceType = typeof(Resources.Common))]
        public DateTime InsertDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DeleteDate { get; set; }
        [Display(Name = "IsActive", ResourceType = typeof(Resources.Common))]
        public bool IsActive { get; set; }
        public DateTime? ActiveDeActiveDate { get; set; }
    }
}