﻿using System.ComponentModel.DataAnnotations;

namespace PublicWebSite.Models
{
    public class MainBaseEntity
    {
        public MainBaseEntity()
        {

        }
        [Key]
        public int Id { get; set; }
    }
}