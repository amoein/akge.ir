﻿using System;
using System.ComponentModel.DataAnnotations;


namespace PublicWebSite.Models
{
    public class ContactUs:BaseEntity
    {

        public ContactUs()
        {
            this.IsViewd = false;
            this.InseDateTime = DateTime.Now;
        }
        [Display(Name = "Title", ResourceType = typeof(Resources.Common))]
        [Required(ErrorMessageResourceName = "TitleRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        public string Title { get; set; }
        [Display(Name = "EmailBody", ResourceType = typeof(Resources.Common))]
        public string Body { get; set; }

         [DataType(DataType.EmailAddress)]
         [Display(Name = "Email", ResourceType = typeof(Resources.Common))]
        public string   Email { get; set; }

        public bool IsViewd { get; set; }

        public DateTime InseDateTime { get; set; }


    }
}