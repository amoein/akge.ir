﻿using System;
using System.ComponentModel.DataAnnotations;


namespace PublicWebSite.Models
{
    public class Link:BaseEntity
    {
        [Display(Name = "Title", ResourceType = typeof(Resources.Common))]
        [Required(ErrorMessageResourceName = "TitleRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        public string Title { get; set; }
        [Display(Name = "LinAddress", ResourceType = typeof(Resources.Common))]
        [Required(ErrorMessageResourceName = "LinkRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        public string LinkAddress { get; set; }
    }
}