﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PublicWebSite.Models
{
    public class UserInfo:MainBaseEntity
    {
        [Display(Name = "Name", ResourceType = typeof(Resources.Common))]
        [Required(ErrorMessageResourceName = "NameRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        public string Name { get; set; }

        [Display(Name = "Family", ResourceType = typeof(Resources.Common))]
        [Required(ErrorMessageResourceName = "FamilyRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        public string Family { get; set; }

        [Display(Name = "UserName", ResourceType = typeof(Resources.Common))]
        [Required(ErrorMessageResourceName = "UserNameRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserEmail", ResourceType = typeof(Resources.Common))]
        public string UserEmail { get; set; }

        [Display(Name = "UserPassWord", ResourceType = typeof(Resources.Common))]
        [Required(ErrorMessageResourceName = "PasswordRequered", ErrorMessageResourceType = typeof(Resources.Common))]
        public string UserPassword { get; set; }

        [Display(Name = "UserType", ResourceType = typeof(Resources.Common))]
        public int UserType { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<UsersBill> UsersBills { get; set; } 


    }
}