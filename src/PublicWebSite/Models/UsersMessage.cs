﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PublicWebSite.Models
{
    public class UsersMessage:MainBaseEntity
    {
        public UsersMessage()
        {
            this.IsViewed = false;
        }
        public bool IsViewed { get; set; }
        public DateTime ViewDate { get; set; }
        public UserInfo UserInfo { get; set; }
        public Message Message { get; set; }
    }
}