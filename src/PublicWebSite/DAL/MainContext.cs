﻿using PublicWebSite.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PublicWebSite.DAL
{
    public class MainContext : DbContext
    {
        public MainContext()
            : base("DefaultConnection")
        {

        }
        public DbSet<Message> Messages { get; set; }
        public DbSet<News> Newses { get; set; }
        public DbSet<ContentPage> ContentPages { get; set; }
        public DbSet<UserInfo> UserInfos { get; set; }
        public DbSet<UsersMessage> UsersMessages { get; set; }
        public DbSet<UsersBill> UsersBills { get; set; }
        public DbSet<UsersPayment> UsersPayments { get; set; }

        public DbSet<SystemConstant> SystemConstants { get; set; }

        public DbSet<Link> Links { get; set; }

        public DbSet<ContactUs> ContactUses { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Entity<Message>()
            //.HasMany(c => c.UserInfos).WithMany(i => i.Messages)
            //.Map(t => t.MapLeftKey("MessageId")
            //    .MapRightKey("UserInfoId")
            //    .ToTable("UserMessage"));
        }
    }
}