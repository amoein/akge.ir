﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PublicWebSite.Models;

namespace PublicWebSite.DAL
{
    public class SimpleWebSiteInitilizer : System.Data.Entity.DropCreateDatabaseIfModelChanges<MainContext>
    {
        protected override void Seed(MainContext context)
        {


            var pageContent = new List<ContentPage>
            {
                new ContentPage {Title = "صفحه اصلی", Body = "صفحه اصلی", PageName = "MainPage"},
                new ContentPage {Title = "درباره ما", Body = "درباره ما", PageName = "AboutUs"},
                new ContentPage {Title = "تماس با ما", Body = "تماس با ما", PageName = "ContactUs"}
            };
            pageContent.ForEach(s => context.ContentPages.Add(s));

            var constant = new SystemConstant()
            {
                Multiply = 2500
            };
            context.SystemConstants.Add(constant);


            context.SaveChanges();


        }
    }
}