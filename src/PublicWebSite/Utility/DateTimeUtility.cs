﻿using System;


namespace PublicWebSite.Utility
{
    public static class DateTimeUtility
    {
        private static System.Globalization.PersianCalendar pcal = new System.Globalization.PersianCalendar();
        public static string StringOfChar(char chr, int Len)
        {
            string result;
            result = "";
            for (int i = 1; i <= Len; i++)
                result = result + chr;
            return result;
        }
        public static string AddZeroToString(String MString, int Len, Boolean ToLeft)
        {
            String StrRepeatChar;
            if ((Len - MString.Length) > 0)
                StrRepeatChar = StringOfChar('0', Len - MString.Length);
            else
                StrRepeatChar = "";
            if (ToLeft)
                return StrRepeatChar + MString;
            else
                return MString + StrRepeatChar;
        }
        public static string GetPersianDate(DateTime gregorianDate)
        {

            try
            {
                return pcal.GetYear(gregorianDate) + "/" + AddZeroToString(pcal.GetMonth(gregorianDate).ToString(), 2, true) + "/" + AddZeroToString(pcal.GetDayOfMonth(gregorianDate).ToString(), 2, true);
            }
            catch
            {
                return "-";
            }
        }
        public static DateTime PersianToGregorian(String SolarDate)
        {
            int Year = 0, Month = 0, Day = 0, Hour = 0, Miniute = 0, Secend = 0, MiliSecond = 0;
            string[] DateSections = SolarDate.Split('/');
            Year = int.Parse(DateSections[2].ToString());
            Month = int.Parse(DateSections[1].ToString());
            Day = int.Parse(DateSections[0].ToString());
            try
            {
                return pcal.ToDateTime(Year, Month, Day, Hour, Miniute, Secend, MiliSecond);
            }
            catch
            {
                return DateTime.Now.Date;
            }
        }
    }
}