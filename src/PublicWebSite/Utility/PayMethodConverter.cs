﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PublicWebSite.Utility
{
    public static class PayMethodConverter
    {
        public  static  string PayMethodToString(int payMethod)
        {
            string retValue = "";
            switch (payMethod)
            {
                case 1:
                    retValue = "پرداخت متفرقه";
                    break;
                case 2:
                    retValue = "حق عضویت";
                    break;
                case 3:
                    retValue = "خرید بارنامه";
                    break;

            }
            return retValue;
        }

        public static string StatusToString(bool status)
        {
            string retValue = "پرداخت نشده";
            if (status)
                retValue = "پرداخت شده";
            return retValue;
        }
    }
}