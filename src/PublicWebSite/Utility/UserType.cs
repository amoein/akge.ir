﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PublicWebSite.Utility
{
    public class UserType
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class UserTypes
    {
        public static List<UserType> GetTypes()
        {
            List<UserType> lsTypes = new List<UserType>
            {
                new UserType(){Id=1,Title = "کاربر میهمان"},
                new UserType(){Id=2,Title = "کاربر مدیر"}
            };
            return lsTypes;
        }
    }

}