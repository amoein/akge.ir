﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PublicWebSite.Utility
{
    public static class BankPaymentUtility
    {
        public static string ConvertBankResCode(BankResCode bankResCode)
        {
            string retValue = "";
            switch (bankResCode)
            {
                case BankResCode.TransactionApproved:
                    retValue = "تراكنش با موفقيت انجام شد";
                    break;
                case BankResCode.InvalidCardNumber:
                    retValue = "شماره كارت نامعتبر است";
                    break;
                case BankResCode.NoSufficientFunds:
                    retValue = "موجودي كافي نيست";
                    break;
                case BankResCode.IncorrectPin:
                    retValue = "رمز نادرست است";
                    break;
                case BankResCode.AllowableNumberOfPinTriesExceeded:
                    retValue = "تعداد دفعات وارد كردن رمز بيش از حد مجاز است";
                    break;
                case BankResCode.CardNotEffective:
                    retValue = "كارت نامعتبر است";
                    break;
                case BankResCode.ExceedsWithdrawalFrequencyLimit:
                    retValue = "دفعات برداشت وجه بيش از حد مجاز است";
                    break;
                case BankResCode.CustomerCancellation:
                    retValue = "كاربر از انجام تراكنش منصرف شده است";
                    break;
                case BankResCode.ExpiredCard:
                    retValue = "تاريخ انقضاي كارت گذشته است";
                    break;
                case BankResCode.ExceedsWithdrawalAmountLimit:
                    retValue = "مبلغ برداشت وجه بيش از حد مجاز است";
                    break;
                case BankResCode.NoSuchIssuer:
                    retValue = "صادر كننده كارت نامعتبر است";
                    break;
                case BankResCode.CardSwitchInternalError:
                    retValue = "خطاي سوييچ صادر كننده كارت";
                    break;
                case BankResCode.IssuerOrSwitchIsInoperative:
                    retValue = "پاسخي از صادر كننده كارت دريافت نشد";
                    break;
                case BankResCode.TransactionNotPermittedToCardHolder:
                    retValue = "دارنده كارت مجاز به انجام اين تراكنش نيست";
                    break;
                case BankResCode.InvalidMerchant:
                    retValue = "پذيرنده نامعتبر است";
                    break;
                case BankResCode.SecurityViolation:
                    retValue = "خطاي امنيتي رخ داده است";
                    break;
                case BankResCode.InvalidUserOrPassword:
                    retValue = "اطلاعات كاربري پذيرنده نامعتبر است";
                    break;
                case BankResCode.InvalidAmount:
                    retValue = "مبلغ نامعتبر است";
                    break;
                case BankResCode.InvalidResponse:
                    retValue = "پاسخ نامعتبر است";
                    break;
                case BankResCode.FormatError:
                    retValue = "فرمت اطلاعات وارد شده صحيح نمي باشد";
                    break;
                case BankResCode.NoInvestmentAccount:
                    retValue = "حساب نامعتبر است";
                    break;
                case BankResCode.SystemInternalError:
                    retValue = "خطاي سيستمي";
                    break;
                case BankResCode.InvalidBusinessDate:
                    retValue = "تاريخ نامعتبر است";
                    break;
                case BankResCode.DuplicateOrderId:
                    retValue = "شماره درخواست تكراري است";
                    break;
                case BankResCode.SaleTransactionNotFound:
                    retValue = "تراکنش یافت نشد";
                    break;
                case BankResCode.DuplicateVerify:
                    retValue = "قبلا درخواست verify داده شده";
                    break;
                case BankResCode.VerifyTransactionNotFound:
                    retValue = "درخواست Verify یافت نشده";
                    break;
                case BankResCode.TransactionHasBeenSettled:
                    retValue = "تراکنش settel  شده است";
                    break;
                case BankResCode.TransactionHasNotBeenSettled:
                    retValue = "تراکنش settel نشده است";
                    break;
                case BankResCode.SettleTransactionNotFound:
                    retValue = "تراکنش settel یافت نشد";
                    break;
                case BankResCode.TransactionHasBeenReversed:
                    retValue = "تراکنش reverse شده است";
                    break;
                case BankResCode.RefundTransactionNotFound:
                    retValue = "تراکنش refound  یافت نشده است";
                    break;
                case BankResCode.BillDigitIncorrect:
                    retValue = "شناسه قبض نادرست است";
                    break;
                case BankResCode.PaymentDigitIncorrect:
                    retValue = "شناسه پرداخت نادرست است";
                    break;
                case BankResCode.BillOrganizationNotValid:
                    retValue = "سازمان صادر كننده قبض نامعتبر است";
                    break;
                case BankResCode.SessionTimeout:
                    retValue = "زمان جلسه كاري به پايان رسيده است";
                    break;
                case BankResCode.DataAccessException:
                    retValue = "خطا در ثبت اطلاعات";
                    break;
                case BankResCode.PayerIdIsInvalid:
                    retValue = "شناسه پرداخت كننده نامعتبر است";
                    break;
                case BankResCode.CustomerNotFound:
                    retValue = "اشكال در تعريف اطلاعات مشتري";
                    break;
                case BankResCode.TryCountExceeded:
                    retValue = "تعداد دفعات ورود اطلاعات از حد مجاز گذشته است";
                    break;
                case BankResCode.InvalidIP:
                    retValue = "IP نامعتبر";
                    break;
                case BankResCode.DuplicateTransmission:
                    retValue = "تراكنش تكراري است";
                    break;
                case BankResCode.OriginalTransactionNotFound:
                    retValue = "تراكنش مرجع موجود نيست";
                    break;
                case BankResCode.InvalidTransaction:
                    retValue = "تراكنش نامعتبر است";
                    break;
                case BankResCode.ErrorInSettle:
                    retValue = "خطا در واريز";
                    break;
                default:
                    retValue = "خطای نامشخص";
                    break;
                    
            }

            return retValue;
        }
    }
}