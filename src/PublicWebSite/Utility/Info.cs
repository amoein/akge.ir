﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PublicWebSite.Models;

namespace PublicWebSite.Utility
{
    public class Info
    {
        
        public Info()
        { }
        public UserInfo user { get; set; }
        public double peayd { get; set; }
        public double notpeayd { get; set; }
        public double rez { get; set; }
    }
}