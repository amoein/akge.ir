﻿using System.Collections.Generic;

namespace PublicWebSite.Utility
{
    public class PayType
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class PayTypes
    {
        public static List<PayType> GetTypes()
        {
            List<PayType> lsTypes = new List<PayType>
            {
               new PayType(){Id=1,Title = "پرداخت متفرقه"},
               new PayType(){Id=2,Title = "حق عضویت"},
              new PayType(){Id=3,Title = "خرید بارنامه"}

            };
            return lsTypes;
        }
    }

}

