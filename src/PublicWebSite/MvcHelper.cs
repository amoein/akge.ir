﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.ComponentModel;

namespace PublicWebSite
{
    public static class MvcHelper
    {
        // ReSharper disable UnusedMember.Global
        // ReSharper disable MemberCanBePrivate.Global
        public static RouteValueDictionary AnonymousObjectToHtmlAttributes(object htmlAttributes)
        {
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
            if (htmlAttributes != null)
            {
                foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(htmlAttributes))
                {
                    routeValueDictionary.Add(propertyDescriptor.Name.Replace('_', '-'), propertyDescriptor.GetValue(htmlAttributes));
                }
            }
            routeValueDictionary.Add("class", "ckeditor");
            return routeValueDictionary;
        }


        private static Dictionary<string, object> GetRowsAndColumnsDictionary(int rows, int columns)
        {
            if (rows < 0)
            {
                throw new ArgumentOutOfRangeException("rows", "Argument is out of range");
            }
            if (columns < 0)
            {
                throw new ArgumentOutOfRangeException("columns", "Argument is out of range");
            }
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            if (rows > 0)
            {
                dictionary.Add("rows", rows.ToString(CultureInfo.InvariantCulture));
            }
            if (columns > 0)
            {
                dictionary.Add("cols", columns.ToString(CultureInfo.InvariantCulture));
            }
            return dictionary;
        }



        private static readonly Dictionary<string, object> implicitRowsAndColumns = new Dictionary<string, object> { { "rows", 2.ToString(CultureInfo.InvariantCulture) }, { "cols", 20.ToString(CultureInfo.InvariantCulture) } };



        private static MvcHtmlString RichTextEditor(HtmlHelper htmlHelper, ModelMetadata modelMetadata, string name, IDictionary<string, object> rowsAndColumns, IDictionary<string, object> htmlAttributes)
        {

            string fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
            if (string.IsNullOrEmpty(fullHtmlFieldName))
            {
                throw new ArgumentException("", "name");
            }
            TagBuilder tagBuilder = new TagBuilder("textarea");
            tagBuilder.GenerateId(fullHtmlFieldName);
            tagBuilder.MergeAttributes(htmlAttributes, true);
            tagBuilder.MergeAttributes(rowsAndColumns, rowsAndColumns != implicitRowsAndColumns);
            tagBuilder.MergeAttribute("name", fullHtmlFieldName, true);
            tagBuilder.MergeAttribute("class", "tinymce", true);
            ModelState modelState;
            if (htmlHelper.ViewData.ModelState.TryGetValue(fullHtmlFieldName, out modelState) && modelState.Errors.Count > 0)
            {
                tagBuilder.AddCssClass(HtmlHelper.ValidationInputCssClassName);
            }
            tagBuilder.MergeAttributes(htmlHelper.GetUnobtrusiveValidationAttributes(name));

            string str;
            if (modelState != null && modelState.Value != null)
            {
                str = modelState.Value.AttemptedValue;
            }
            else
            {
                str = modelMetadata.Model != null ? modelMetadata.Model.ToString() : string.Empty;
            }

            tagBuilder.SetInnerText(Environment.NewLine + str);
            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.Normal));
        }


        public static MvcHtmlString RichTextEditor(this HtmlHelper htmlHelper, string name)
        {
            return RichTextEditor(htmlHelper, name, new object());
        }

        public static MvcHtmlString RichTextEditor(this HtmlHelper htmlHelper, string name, object htmlAttributes)
        {
            return RichTextEditor(htmlHelper, name, AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString RichTextEditor(this HtmlHelper htmlHelper, string name, IDictionary<string, object> htmlAttributes)
        {
            if (htmlAttributes != null)
            {
                htmlAttributes.Add("class", "tinymce");
            }
            else
            {
                htmlAttributes = new RouteValueDictionary { { "class", "tinymce" } };
            }
            return htmlHelper.TextArea(name, null, htmlAttributes);
        }

        public static MvcHtmlString RichTextEditor(this HtmlHelper htmlHelper, string name, string value)
        {
            return RichTextEditor(htmlHelper, name, value, null);
        }

        public static MvcHtmlString RichTextEditor(this HtmlHelper htmlHelper, string name, string value, object htmlAttributes)
        {
            return RichTextEditor(htmlHelper, name, value, AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString RichTextEditor(this HtmlHelper htmlHelper, string name, string value, IDictionary<string, object> htmlAttributes)
        {
            ModelMetadata modelMetadata = ModelMetadata.FromStringExpression(name, htmlHelper.ViewContext.ViewData);
            if (value != null)
            {
                modelMetadata.Model = value;
            }
            return RichTextEditor(htmlHelper, modelMetadata, name, implicitRowsAndColumns, htmlAttributes);
        }

        public static MvcHtmlString RichTextEditor(this HtmlHelper htmlHelper, string name, string value, int rows, int columns, object htmlAttributes)
        {
            return RichTextEditor(htmlHelper, name, value, rows, columns, AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString RichTextEditor(this HtmlHelper htmlHelper, string name, string value, int rows, int columns, IDictionary<string, object> htmlAttributes)
        {
            ModelMetadata modelMetadata = ModelMetadata.FromStringExpression(name, htmlHelper.ViewContext.ViewData);
            if (value != null)
            {
                modelMetadata.Model = value;
            }
            return RichTextEditor(htmlHelper, modelMetadata, name, GetRowsAndColumnsDictionary(rows, columns), htmlAttributes);
        }

        //public static MvcHtmlString CKEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        //{
        //    if (expression == null)
        //    {
        //        throw new ArgumentNullException("expression");
        //    }
        //    RouteValueDictionary a = new RouteValueDictionary();
        //    return CKEditorFor<TModel, TProperty>(htmlHelper, ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData), a);
        //}

        public static MvcHtmlString RichTextEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            return RichTextEditor(htmlHelper, ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData), ExpressionHelper.GetExpressionText(expression), implicitRowsAndColumns, htmlAttributes);
        }

        public static MvcHtmlString RichTextEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, int rows, int columns, object htmlAttributes)
        {
            return RichTextEditorFor(htmlHelper, expression, rows, columns, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        public static MvcHtmlString RichTextEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, int rows, int columns, IDictionary<string, object> htmlAttributes)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            return RichTextEditor(htmlHelper, ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData), ExpressionHelper.GetExpressionText(expression), GetRowsAndColumnsDictionary(rows, columns), htmlAttributes);
        }




        public static MvcHtmlString ImageActionLink(
           this HtmlHelper helper,
           string linkText,
           string imageUrl,
           string altText,
           string href,
           object routeValues,
           object linkHtmlAttributes,
           object imgHtmlAttributes)
        {
            var linkAttributes = AnonymousObjectToKeyValue(linkHtmlAttributes);
            var imgAttributes = AnonymousObjectToKeyValue(imgHtmlAttributes);
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", altText);
            imgBuilder.MergeAttributes(imgAttributes, true);
            var linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", href);
            linkBuilder.MergeAttributes(linkAttributes, true);
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += linkText;
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }




        public static MvcHtmlString ImageActionLink(
            this HtmlHelper helper,
            string linkText,
            string imageUrl,
            string altText,
            string actionName,
            string controllerName,
            object routeValues,
            object linkHtmlAttributes,
            object imgHtmlAttributes)
        {
            var linkAttributes = AnonymousObjectToKeyValue(linkHtmlAttributes);
            var imgAttributes = AnonymousObjectToKeyValue(imgHtmlAttributes);
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", altText);
            imgBuilder.MergeAttributes(imgAttributes, true);
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, controllerName, routeValues));
            linkBuilder.MergeAttributes(linkAttributes, true);
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += linkText;
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }


        public static MvcHtmlString ImageActionLink(
           this HtmlHelper helper,
           string linkText,
           string imageUrl,
           string altText,
           string actionName,
           string controllerName
           )
        {
            return ImageActionLink(helper, linkText, imageUrl, altText, actionName, controllerName, new object(), new object(), new object());
        }


        public static MvcHtmlString ImageActionLink(
          this HtmlHelper helper,
          string linkText,
          string imageUrl,
          string altText,
          string actionName

          )
        {
            return ImageActionLink(helper, linkText, imageUrl, altText, actionName, string.Empty, new object(), new object(), new object());
        }

        //public static MvcHtmlString ImageActionLink(
        //    this HtmlHelper helper,
        //    string imageUrl,
        //    string altText,
        //    string actionName,
        //    object routeValues,
        //    object imgHtmlAttributes)
        //{
        //    var imgAttributes = AnonymousObjectToKeyValue(imgHtmlAttributes);
        //    var imgBuilder = new TagBuilder("img");
        //    imgBuilder.MergeAttribute("src", imageUrl);
        //    imgBuilder.MergeAttribute("alt", altText);
        //    imgBuilder.MergeAttributes(imgAttributes, true);
        //    var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
        //    var linkBuilder = new TagBuilder("a");
        //    linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, routeValues));
        //    var text = linkBuilder.ToString(TagRenderMode.StartTag);
        //    text += imgBuilder.ToString(TagRenderMode.SelfClosing);
        //    text += linkBuilder.ToString(TagRenderMode.EndTag);
        //    return MvcHtmlString.Create(text);
        //}

        //public static MvcHtmlString ImageActionLink(
        //    this HtmlHelper helper,
        //    string imageUrl,
        //    string altText,
        //    string actionName,
        //    object routeValues)
        //{
        //    var imgBuilder = new TagBuilder("img");
        //    imgBuilder.MergeAttribute("src", imageUrl);
        //    imgBuilder.MergeAttribute("alt", altText);
        //    var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
        //    var linkBuilder = new TagBuilder("a");
        //    linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, routeValues));
        //    var text = linkBuilder.ToString(TagRenderMode.StartTag);
        //    text += imgBuilder.ToString(TagRenderMode.SelfClosing);
        //    text += linkBuilder.ToString(TagRenderMode.EndTag);
        //    return MvcHtmlString.Create(text);
        //}

        //public static MvcHtmlString ImageActionLink(
        //    this HtmlHelper helper,
        //    string imageUrl,
        //    string altText,
        //    string actionName)
        //{
        //    var imgBuilder = new TagBuilder("img");
        //    imgBuilder.MergeAttribute("src", imageUrl);
        //    imgBuilder.MergeAttribute("alt", altText);
        //    var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
        //    var linkBuilder = new TagBuilder("a");
        //    linkBuilder.MergeAttribute("href", urlHelper.Action(actionName));
        //    var text = linkBuilder.ToString(TagRenderMode.StartTag);
        //    text += imgBuilder.ToString(TagRenderMode.SelfClosing);
        //    text += linkBuilder.ToString(TagRenderMode.EndTag);
        //    return MvcHtmlString.Create(text);
        //}

        private static Dictionary<string, object> AnonymousObjectToKeyValue(object anonymousObject)
        {
            var dictionary = new Dictionary<string, object>();
            if (anonymousObject != null)
            {
                foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(anonymousObject))
                {
                    dictionary.Add(propertyDescriptor.Name, propertyDescriptor.GetValue(anonymousObject));
                }
            }
            return dictionary;
        }







        public static IHtmlString ImageActionLink(this AjaxHelper helper, string imageUrl,
                                                    string altText, string actionName,
                                                    object routeValues, AjaxOptions ajaxOptions, object htmlAttributes = null)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", imageUrl);
            builder.MergeAttribute("alt", altText);
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            var link = helper.ActionLink("[replaceme]", actionName, routeValues, ajaxOptions).ToHtmlString();
            return MvcHtmlString.Create(link.Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing)));
        }

        // ReSharper restore UnusedMember.Global
        // ReSharper restore MemberCanBePrivate.Global
    }
}