﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using PublicWebSite.Models;
using PublicWebSite.DAL;

namespace PublicWebSite.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class UsersController : Controller
    {
        private MainContext db = new MainContext();

        // GET: /Users/
        public virtual ActionResult Index(string sortOrder, int? page ,int? id)
        {
            ViewBag.UserNameSortParm = String.IsNullOrEmpty(sortOrder) ? "username_desc" : "";
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.Family = String.IsNullOrEmpty(sortOrder) ? "family_desc" : "";
            
            var lstusers = from s in db.UserInfos
                           where s.UserName != User.Identity.Name
                              select s;

            switch (sortOrder)
            {
                case "username_desc":
                    lstusers = lstusers.OrderByDescending(s => s.UserName);
                    break;
                case "name_desc":
                    lstusers = lstusers.OrderByDescending(s => s.Name);
                    break;
                case "family_desc":
                    lstusers = lstusers.OrderByDescending(s => s.Family);
                    break;
                default:
                    lstusers = lstusers.OrderBy(s => s.UserName);
                    break;
            }

            const int pageSize = 10;
            int pageNumber = (page ?? 1);

            if (id == 1)
            {

                ViewBag.massage1 = "1";
            }
            if (id == 0)
            {

                ViewBag.massage1 = "0";
            }

            return View(lstusers.ToPagedList(pageNumber, pageSize));
        }


        // GET: /Users/Create
        public virtual ActionResult Create(int? idi)
        {
            if(idi==1)
            { ViewBag.alt = idi; }
            ViewBag.UserTypes = new SelectList(Utility.UserTypes.GetTypes(),"Id","Title");
            return View();
        }

        // POST: /Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "Id,Name,Family,UserName,UserPassword,UserEmail")] UserInfo userinfo)
        {
            if(! db.UserInfos.Any(c=> c.UserName==userinfo.UserName) )
            { 
            if (ModelState.IsValid)
            {
                string userRole = "Guest";
                userinfo.UserType = 1;
                Membership.CreateUser(userinfo.UserName, userinfo.UserPassword,userinfo.UserEmail);
                Roles.AddUserToRole(userinfo.UserName, userRole);
                db.UserInfos.Add(userinfo);
                db.SaveChanges();
                return RedirectToAction("Index",routeValues: new { id = 0 });
            }

            return View(userinfo);
            }

            return RedirectToAction("Create", routeValues: new { idi = 1 });
           

        }

        // GET: /Users/Edit/5
        public virtual ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo userinfo = db.UserInfos.Find(id);
            if (userinfo == null)
            {
                return HttpNotFound();
            }
            return View(userinfo);
        }

        // POST: /Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit(int Id, string Name, string Family, string UserEmail)
        {
            UserInfo userinfo = db.UserInfos.FirstOrDefault(c => c.Id == Id);
            userinfo.Name = Name;
            userinfo.Family = Family;
            userinfo.UserEmail = UserEmail;

            if (ModelState.IsValid)
            {
                db.Entry(userinfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", routeValues: new { id = 0 });
            }
            return View(userinfo);
        }

        // GET: /Users/Delete/5
        public virtual ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            


            if (db.Messages.Any(p => p.user_id == id))
            {
                return (RedirectToAction(actionName: "Index",
            routeValues: new { id= 1 }));
            }


            UserInfo userinfo = db.UserInfos.Find(id);
            if (userinfo == null)
            {
                return HttpNotFound();
            }
            return View(userinfo);
        }

        // POST: /Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(int id)
        {
            if (db.UsersMessages.Any(p => p.UserInfo.Id == id))
            {
                return (RedirectToAction(actionName: "Index",
            routeValues: new { id = 1 }));
            }
          //  if (db.UsersBills.Any(p => p.UserInfo.Id == id))
            //{
              //  return (RedirectToAction(actionName: "Index",
            //routeValues: new { id = 1 }));
            //}
            
            UserInfo userinfo = db.UserInfos.Find(id);
            Membership.DeleteUser(userinfo.UserName);
            db.UserInfos.Remove(userinfo);
            db.SaveChanges();

            return RedirectToAction("Index", routeValues: new { id = 0 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
