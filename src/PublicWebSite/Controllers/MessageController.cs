﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PublicWebSite.Models;
using PublicWebSite.DAL;
using System.Media;
using System.Web.Util;
using System.IO;
using PublicWebSite.Utility;

namespace PublicWebSite.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class MessageController : Controller
    {
        private MainContext db = new MainContext();

        // GET: /Message/
        public virtual ActionResult Index(string sortOrder, int? page, int? idi)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            
            UserInfo model = new UserInfo();

            model = db.UserInfos.Where(c => c.UserName == User.Identity.Name).FirstOrDefault();


            var lstMessages = from s in db.Messages
                              where s.user_id == model.Id
                             select s;

            switch (sortOrder)
            {
                case "name_desc":
                    lstMessages = lstMessages.OrderByDescending(s => s.Title);
                    break;
                case "Date":
                    lstMessages = lstMessages.OrderByDescending(s => s.InsertDate);
                    break;
                case "date_desc":
                    lstMessages = lstMessages.OrderBy(s => s.Title);
                    break;
                default:
                    lstMessages = lstMessages.OrderBy(s => s.Title);
                    break;
            }
            const int pageSize = 10;
            int pageNumber = (page ?? 1);


            if (idi == 1)
            {

                ViewBag.massage1 = "1";
            }
            if (idi == 0)
            {

                ViewBag.massage1 = "0";
            }
            return View(lstMessages.ToPagedList(pageNumber, pageSize));
        }

        public virtual ActionResult SendMessage(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            ViewBag.MessageBody = message.Body;
            ViewBag.MessageTitle = message.Title;
            ViewBag.MessageId = message.Id;
            var userInfos = db.UserInfos.Where(c => c.Id != message.user_id);
            return View(userInfos);

        }

        [HttpPost]
        public virtual ActionResult SendMessage(FormCollection frmCollection, string messageid)
        {
            

            int intmessageid = int.Parse(messageid);
            var message = db.Messages.Find(intmessageid);
            var chckedValues = frmCollection.GetValues("assignChkBx");

            
            if (chckedValues != null)
                foreach (var id in chckedValues)
                {
                    int userId = int.Parse(id);
                    var user = db.UserInfos.Find(userId);

                    UsersMessage usersMessage=new UsersMessage();
                    usersMessage.Message = message;
                    usersMessage.UserInfo = user;
                    usersMessage.ViewDate = DateTime.Now;
                    db.UsersMessages.Add(usersMessage);
                }
            db.SaveChanges();
            return RedirectToAction("index", routeValues: new { idi = 0 }
                );
        }


        // GET: /Message/Create
        public virtual ActionResult Create()
        {
            ViewBag.Id = db.Messages.LastOrDefault().Id;
            return View();
        }

        /*public virtual ActionResult Create(string name, Message message)
        {
             message.Body = message.Body + "<p><img src=\"~/MessageAttach/"+name+"\" /> </p>";
                return View(message);

        }*/
     
        // POST: /Message/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "Id,Body,Title,IsViewed,ViewDate,InsertDate,UpdateDate,IsDeleted,DeleteDate,IsActive,ActiveDeActiveDate")] Message message )
        {
            

            if (ModelState.IsValid)
            {
                UserInfo model = new UserInfo();

                model = db.UserInfos.Where(c => c.UserName == User.Identity.Name).FirstOrDefault();

                message.user_id = model.Id;
                message.Body = message.Body ;
                db.Messages.Add(message);
                db.SaveChanges();


                return RedirectToAction("Index");
            }

            return View(message);
        }

        // GET: /Message/Edit/5
        public virtual ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }




        // POST: /Message/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "Id,Body,Title,IsViewed,ViewDate,InsertDate,UpdateDate,IsDeleted,DeleteDate,IsActive,ActiveDeActiveDate")] Message message)
        {
            message.user_id = db.UserInfos.Where(c => c.UserName == User.Identity.Name).FirstOrDefault().Id;
            if (ModelState.IsValid)
            {

                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(message);
        }

        // GET: /Message/Delete/5
        public virtual ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.UsersMessages.Any(p => p.Message.Id == id))
            {
                return (RedirectToAction(actionName: "Index",
            routeValues: new { idi = 1 }));
            }

            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // POST: /Message/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(int id)
        {
            if (db.UsersMessages.Any(p => p.Message.Id ==id))
            {
                return (RedirectToAction(actionName: "Index",
            routeValues: new { idi = 0 }));
            }
            Message message = db.Messages.Find(id);
            db.Messages.Remove(message);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
