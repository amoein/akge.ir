﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PublicWebSite.Utility;

namespace PublicWebSite.Controllers
{
    public class FileController : Controller
    {
        public virtual ActionResult Index(PublicWebSite.Models.Message s)
        {
            PublicWebSite.Models.Message n=new  PublicWebSite.Models.Message();
            ViewBag.message  =s;
            return View(n);
        }

        /// <summary>
        /// Uploads the file.
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        public ContentResult UploadFiles()
        {

            var r = new List<UploadFilesResult>();

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;

                string savedFileName = Path.Combine(Server.MapPath("~/MessageAttach"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);

                r.Add(new UploadFilesResult()
                {
                    Name = hpf.FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }

    }
}