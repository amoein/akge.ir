﻿using PublicWebSite.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PublicWebSite.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class SystemConstantController : Controller
    {
        private MainContext db = new MainContext();
        //
        // GET: /SystemConstant/
        public virtual ActionResult Index()
        {
            var systemConstants= db.SystemConstants.FirstOrDefault();
            return View(systemConstants);
        }

        [HttpPost]
        public virtual ActionResult Index(int multiply)
        {
            var systemConstants = db.SystemConstants.FirstOrDefault();
            systemConstants.Multiply = multiply;
            db.SaveChanges();
            return View(systemConstants);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}