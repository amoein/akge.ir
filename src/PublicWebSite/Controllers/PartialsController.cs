﻿using PublicWebSite.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PublicWebSite.Controllers
{
    public partial class PartialsController : Controller
    {
        private MainContext db = new MainContext();
        public virtual ActionResult LogOnPartial()
        {
            return PartialView("_LogInPartial");
        }
        public virtual ActionResult LoadNews()
        {

            var news = db.Newses;
            return PartialView("_LoadNews",news);
        }

        public virtual ActionResult LoadLinks()
        {
            var links = db.Links;
            return PartialView("_LoadLinks", links);
        }
        public virtual ActionResult LoadArticles()
        {

            var articles = db.ContentPages.Where(c=>c.Id>3);
            return PartialView("_LoadArticles", articles);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}