﻿using PublicWebSite.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PublicWebSite.Controllers
{
    public partial class ContactUsListController : Controller
    {
        private MainContext db = new MainContext();
        //
        // GET: /ContactUsList/
        public virtual ActionResult Index()
        {

            return View(db.ContactUses.OrderByDescending(c=>c.InseDateTime));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}