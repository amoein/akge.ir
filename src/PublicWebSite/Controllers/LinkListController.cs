﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PublicWebSite.DAL;
using PublicWebSite.Models;
using System.Net;

namespace PublicWebSite.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class LinkListController : Controller
    {
        private MainContext db = new MainContext();
        //
        // GET: /LinkList/
        public virtual ActionResult Index()
        {
            return View(db.Links);
        }
        public virtual ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult Create(Link link)
        {
            db.Links.Add(new Link() {LinkAddress = link.LinkAddress, Title = link.Title});
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public virtual ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Link link = db.Links.Find(id);
            if (link == null)
            {
                return HttpNotFound();
            }
            db.Links.Remove(link);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}