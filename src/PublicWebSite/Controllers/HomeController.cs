﻿using PagedList;
using PublicWebSite.DAL;
using PublicWebSite.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;


namespace PublicWebSite.Controllers
{
    
    [Authorize(Roles = "Admin")]
    public partial class HomeController : Controller
    {
        private MainContext db = new MainContext();

        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult MainPage()
        {
            var mainpage = db.ContentPages.FirstOrDefault(c => c.PageName == "MainPage");
            
            return View(mainpage);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult MainPage([Bind(Include = "Id,Body,Title,PageName")] ContentPage contentPage)
        {
            if (ModelState.IsValid)
            {
                contentPage.PageName = "MainPage";
                db.Entry(contentPage).State = EntityState.Modified;
                db.SaveChanges();
               
            }
            return View(contentPage);
        }
        public virtual ActionResult About()
        {
            var aboutUs = db.ContentPages.FirstOrDefault(c => c.PageName == "AboutUs");

            return View(aboutUs);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult About([Bind(Include = "Id,Body,Title,PageName")] ContentPage contentPage)
        {
            if (ModelState.IsValid)
            {
                contentPage.PageName = "AboutUs";
                db.Entry(contentPage).State = EntityState.Modified;
                db.SaveChanges();

            }
            return View(contentPage);
        }

        public virtual ActionResult Contact()
        {
            var contactUs = db.ContentPages.FirstOrDefault(c => c.PageName == "ContactUs");

            return View(contactUs);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Contact([Bind(Include = "Id,Body,Title,PageName")] ContentPage contentPage)
        {
            if (ModelState.IsValid)
            {
                contentPage.PageName = "ContactUs";
                db.Entry(contentPage).State = EntityState.Modified;
                db.SaveChanges();

            }
            return View(contentPage);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}