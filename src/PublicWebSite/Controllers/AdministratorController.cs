﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PublicWebSite.Controllers
{
    public partial class AdministratorController : Controller
    {
        // GET: Administrator
        private PublicWebSite.DAL.MainContext db = new PublicWebSite.DAL.MainContext();
        public virtual ActionResult setadmin()
        {
            if (!db.UserInfos.Any(c => c.UserName == "admin"))
            {
                PublicWebSite.Models.UserInfo adminuser = new PublicWebSite.Models.UserInfo();
                adminuser.Id = 1;
                adminuser.Name = "مدیر";
                adminuser.Family = "سرپرست";
                adminuser.UserName = "admin";
                adminuser.UserPassword = "qweasd";
                adminuser.UserEmail = "info@akge.ir";
                adminuser.UserType = 2;


                if (!System.Web.Security.Roles.RoleExists("Guest"))
                {
                    System.Web.Security.Roles.CreateRole("Guest");
                    System.Web.Security.Roles.CreateRole("Admin");

                    System.Web.Security.Membership.CreateUser(adminuser.UserName, adminuser.UserPassword, adminuser.UserEmail);

                    string userRole = "Admin";

                    System.Web.Security.Roles.AddUserToRole(adminuser.UserName, userRole);
                }

                db.UserInfos.Add(adminuser);
                db.SaveChanges();
            }
            return RedirectToAction("index", "main");
        }
    }
}