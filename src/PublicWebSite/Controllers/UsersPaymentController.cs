﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using PublicWebSite.Models;
using PublicWebSite.DAL;
using System.Collections.Generic;
using PublicWebSite.Utility;

namespace PublicWebSite.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class UsersPaymentController : Controller
    {
       

        private MainContext db = new MainContext();
        //
        // GET: /UsersPayment/
        public virtual ActionResult Index()
        {
            var users = db.UserInfos.Where(c => c.UserName !="admin" ).ToList();


            List<PublicWebSite.Utility.Info> info = new List<PublicWebSite.Utility.Info>();

           foreach (UserInfo item in users)
           {
              
                   PublicWebSite.Utility.Info temp = new Info();
                   temp.user = item;

                   if (db.UsersPayments.Any(c => c.UserInfo_Id == item.Id))
                   {
                   var s = db.UsersPayments.Where(c => c.UserInfo_Id == item.Id).Select(c => c.Amount);
                   temp.peayd = (from a in s
                                 select a)
                                .Sum();
                   }
                    else
                   {
                       temp.peayd=0;
                   }

                   if (db.UsersBills.Any(c => c.UserInfo_Id == item.Id))
                   {
                       temp.notpeayd = db.UsersBills.Where(c => c.UserInfo_Id == item.Id).Sum(a => a.Sum);
                   }


                   else
                   {
                       temp.notpeayd = 0;
                   }

                   temp.rez = temp.peayd - temp.notpeayd;

                   info.Add(temp);
               
           }
            
            
            return View(info);
        }

        public virtual ActionResult Details()
        {
            return View();
        }

        public virtual ActionResult DetailsOfPay(int id)
        {
            var userInfo = db.UserInfos.FirstOrDefault(c => c.Id == id);
            
            ViewBag.UserNameFamil = string.Format("{0} {1}", userInfo.Name, userInfo.Family);
           
            var userPayments = db.UsersPayments.Where(c => c.UserInfo_Id == id).OrderByDescending(c=>c.PayedDate);
         
            if (userPayments == null)
            { return new EmptyResult(); }

            return View(userPayments);
            
        }

        public virtual ActionResult DetailsOfbill(int id)
        {
            var userInfo = db.UserInfos.FirstOrDefault(c => c.Id == id);

            ViewBag.UserNameFamil = string.Format("{0} {1}", userInfo.Name, userInfo.Family);

            var bill = db.UsersBills.Where(c => c.UserInfo_Id == id).OrderByDescending(c => c.InsertDate);
         
            if (bill == null)
            { return new EmptyResult(); }
            return View(bill);

        }
        public virtual ActionResult New(int id)
        {
            ViewBag.UserTypes = new SelectList(Utility.PayTypes.GetTypes(), "Id", "Title");
            UserInfo userInfo = db.UserInfos.FirstOrDefault(c => c.Id == id);
            if (userInfo != null)
            {
                ViewBag.Id = id;
                ViewBag.UserName = string.Format("{0}-{1}", userInfo.Name, userInfo.Family);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult New(string amount, int id, int payType, string title)
        {
            var user = db.UserInfos.Find(id);
           
            int intAmount = 0;
            int.TryParse(amount.Replace(",",""), out intAmount);

            //ToDo:Erro Cheking
            #region Check PayType and Caculate
            var constPayTypes = db.SystemConstants.FirstOrDefault();

            if (constPayTypes == null)
            {
                constPayTypes = new SystemConstant();  
                constPayTypes.Multiply = 1;
                RedirectToAction("~/SystemConstant");
            }

            double sum = 0;

            if (payType == 1)
            { 
                sum = intAmount * constPayTypes.Multiply; }
            else
            {
                sum = intAmount;}

            #endregion
            //now we have som and multiply

            #region If UserBill is already inserted update with new one else insert new one
         
           
                db.UsersBills.Add(new UsersBill() {Title = title, Sum = sum, UserInfo_Id = id});
           

            db.SaveChanges(); 
            #endregion

           

            ViewBag.UserTypes = new SelectList(Utility.PayTypes.GetTypes(), "Id", "Title");
            if (user != null)
            {
                ViewBag.Id = id;
                ViewBag.UserName = string.Format("{0}-{1}", user.Name, user.Family);
                ViewBag.PayResult = string.Format("به مبلغ {0}به حساب کاربر اضافه گردید شد.", sum.ToString("##,###"));


            }
           
            return View();
            
        }

        public virtual ActionResult DeleteUserPay(int id)
        {
            var UserPay = db.UsersPayments.FirstOrDefault(c => c.Id == id);
            return View(UserPay);
        }
        /*
        // POST: /Message/Delete/5
        [HttpPost, ActionName("DeleteUserPay")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(int id)
        {

            var userPayment = db.UsersPayments.Find(id);
          var userBill = db.UsersBills.Find(userPayment.UsersBill_Id);

            
           
            userBill.Sum -= userPayment.Amount;
            db.UsersPayments.Remove(userPayment);
            db.SaveChanges();
            return RedirectToAction("Details/"+userBill.UserInfo_Id);
        }
        */


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}