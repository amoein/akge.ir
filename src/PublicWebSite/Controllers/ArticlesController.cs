﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PublicWebSite.Models;
using PublicWebSite.DAL;

namespace PublicWebSite.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class ArticlesController : Controller
    {
        private MainContext db = new MainContext();

        // GET: /Articles/
        public virtual ActionResult Index()
        {
            return View(db.ContentPages.Where(c=>c.Id>3).ToList());
        }

        // GET: /Articles/Details/5
        public virtual ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentPage contentpage = db.ContentPages.Find(id);
            if (contentpage == null)
            {
                return HttpNotFound();
            }
            return View(contentpage);
        }

        // GET: /Articles/Create
        public virtual ActionResult Create()
        {
            return View();
        }

        // POST: /Articles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "Id,Title,Body,KeyWords,PageName")] ContentPage contentpage)
        {
                contentpage.KeyWords = "";
                contentpage.PageName = "";
                db.ContentPages.Add(contentpage);
                db.SaveChanges();
                return RedirectToAction("Index");
           

           
        }

        // GET: /Articles/Edit/5
        public virtual ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentPage contentpage = db.ContentPages.Find(id);
            if (contentpage == null)
            {
                return HttpNotFound();
            }
            return View(contentpage);
        }

        // POST: /Articles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "Id,Title,Body,KeyWords,PageName")] ContentPage contentpage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contentpage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contentpage);
        }

        // GET: /Articles/Delete/5
        public virtual ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentPage contentpage = db.ContentPages.Find(id);
            if (contentpage == null)
            {
                return HttpNotFound();
            }
            return View(contentpage);
        }

        // POST: /Articles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(int id)
        {
            ContentPage contentpage = db.ContentPages.Find(id);
            db.ContentPages.Remove(contentpage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
