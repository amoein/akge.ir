﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PublicWebSite.Startup))]
namespace PublicWebSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
